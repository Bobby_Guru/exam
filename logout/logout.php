<?php

	if (isset($_POST['end'])) {
		
		session_destroy();
		header('location: ../index.php');
		exit();
	} else {

		if (isset($_GET['out'])) {
			
			session_destroy();
			header('location: ../index.php');
			exit();
		}
	}