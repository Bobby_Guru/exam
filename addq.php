<!DOCTYPE html>
<html>
<head>
	<title>Add Questions</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">

	<nav>
		<ul class="w3-navbar w3-black">
			<li><a href="adminPanel.php">View Results</a></li>
			<li><a href="addq.php">Add Questions</a></li>
			<li><a href="viewq.php">View Questions</a></li>
			<li><a href="">Update Profile</a></li>
			<li><a href="logout/logout.php?out">Logout</a></li>
		</ul>
	</nav>
</head>
<body class="w3-container">

	<div id="about-container w3-container">
		<h1 class="qHead">Admin C Panel</h1>

		<h4 class="qHead">Add Questions</h4>

		<form action="questions/insertquestion.php" method="POST">

			<div>
				<label>Questions</label> <br>
				<textarea cols="50" rows="5" name="questions" class="questions w3-mobile"></textarea>
			</div> 

			<div>
				<label>Option One</label> <br>
				<input class="w3-input w3-border" type="text" name="optionOne" placeholder="Option One">
			</div>  <br>

			<div>
				<label>Option Two</label> <br>
				<input class="w3-input w3-border" type="text" name="optionTwo" placeholder="Option Two">
			</div> <br>

			<div>
				<label>Option Three</label> <br>
				<input class="w3-input w3-border" type="text" name="optionThree" placeholder="Option Three">
			</div> <br>

			<div>
				<label>Option Four</label> <br>
				<input class="w3-input w3-border" type="text" name="optionFour" placeholder="Option Four">
			</div> <br>

			<div> 
				<label>Answer</label> <br>
				<input class="w3-input w3-border" type="text" name="ans" placeholder="Answer">
			</div> <br> <br>

			<button class="w3-btn w3-teal w3-border w3-border-tear w3-round-large" type="submit" name="ques">Add Questions</button> <br> <br>
		</form>

	</div>


</body>
</html>