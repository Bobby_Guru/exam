<?php

	include "inc/fetch.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">

	<nav>
		<ul class="w3-navbar w3-black">
			<li><a href="adminPanel.php">View Results</a></li>
			<li><a href="addq.php">Add Questions</a></li>
			<li><a href="viewq.php">View Questions</a></li>
			<li><a href="">Update Profile</a></li>
			<li><a href="logout/logout.php?out">Logout</a></li>
		</ul>
	</nav>
</head>
<body>

	<div class="w3-container">

		<?php 
			$date = Date('d/m/y');
			$time = Date('h:i:sa');
			print  ('Today\'s Date is: ').$date. ('<br>');
			print ('The Time is: '). $time;
		?>
		
		<h3>Admin C Panel</h3>

		<!--////////// Where the table starts///////////////////////////-->

		<div class="w3-responsive">
			<table class="w3-table w3-striped w3-bordered w3-border w3-hoverable">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Score</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = mysqli_fetch_array($bind)) {
					?>
					<tr>
						<td><?php print $row['firstName']; ?></td>
						<td><?php print $row['lastName']; ?></td>
						<td><?php print $row['grade']; ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	<!--////////// Where the table ends///////////////////////////-->
		

</body>
</html>