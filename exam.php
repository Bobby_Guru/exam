<?php
	require_once "inc/server.php";
	require_once "inc/login.php";

	if (isset($_GET['success'])) {
		
		$id = $_GET['success'];
		$sql = "SELECT * FROM student WHERE firstName='$id'";
		$bind = mysqli_query($dbConnection, $sql) or die('Error from bind on line 8');
		$record = mysqli_fetch_array($bind);

		$name = $record['firstName'];
		$last = $record['lastName'];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Exam</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">
</head>
<body>

	<?php
		if (isset($_SESSION['welcome'])) {?>
			<center>
			<div class="suc">
				<?php
					print $_SESSION['welcome'];
					unset($_SESSION['welcome']);
				?>
			</div>
			</center>
	<?php }?>

	<header class="w3-container w3-red w3-center">
		<h2 class="Ehead">Examination Questions</h2>
		<h3>Welcome <?php print $name. ' ' .$last ?></h3>
	</header>

	<?php

			$Server = 'localhost';
			$User = 'root';
			$Password = '';
			$DBname = 'exam';


			$db = mysqli_connect($Server, $User, $Password, $DBname) or die('connection error');

			if (isset($_POST['result'])) {

				//select everything from ans question table
				$getscore = "SELECT * FROM questions";
				$collect = mysqli_query($db, $getscore) or die('collect error on line 60');

				//connect the sql cquery
				$result = mysqli_num_rows($collect);

				//to get the choice of the user
				$useranswer = $_POST['option'];
				//variable to increase the score if correct answer is chosen
				$correct = 0;

				if ($result == true || isset($_GET['name']) || isset($_GET['success'])) {

					//$name = $_GET['name'];
					$name = $_GET['success'];
					//$edit_state = true;

					$rec = mysqli_query($db, "SELECT * FROM student WHERE firstName='$name'");
					$record = mysqli_fetch_array($rec);

					$id = $record['id'];

					//updates user answer row in info table in database
					$update = "UPDATE student SET user_ans = '$useranswer' WHERE id=$id";
					$coll = mysqli_query($db, $update) or die('coll error on line 82');

					//fetch all the correct answers from database
					$fetch = "SELECT answer FROM questions";
					$bindF = mysqli_query($db, $fetch) or die('error from Bind F on line 84');

					//check if the answer the user selected is correct.
					while ($row = mysqli_fetch_array($bindF)) {
						
						if ($row['answer'] == $useranswer) {
							
							$ansSQL = "UPDATE student SET grade=grade+1 WHERE id=$id";
							$ansBind = mysqli_query($db, $ansSQL) or die('error from Ans Bind on line 92');
						}

					}
				}
			}
	?>

	<div class="w3-container">

		<form class="Eform" action="" method="POST">

			<?php
				$Server = 'localhost';
				$User = 'root';
				$Password = '';
				$DBname = 'exam';

				$db = mysqli_connect($Server, $User, $Password, $DBname) or die('connection error');

				$per_page = 1;// number of page you want to display

				//gets the id from the database.
				$page_query = "SELECT 1 FROM questions;";

				//binds the above query with the database connection.
				$bind = mysqli_query($db, $page_query) or die('bind error on line 65');

				//the zero selects the first row as array starts from zero. and we divide it by the page_query. this vairablw holds the amount of pages available
				$pages = ceil(mysqli_num_rows($bind)/$per_page);

				// a short way to write a for loop to get 
				$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

				$start = ($page - 1) * $per_page;
				$sql = "SELECT * FROM questions ORDER BY RAND() LIMIT $start, $per_page";
				$query = mysqli_query($db, $sql) or die('query error');

				while ($query_row = mysqli_fetch_assoc($query)) {

					print $query_row['question'].'<br />' . '<br/>';

					print ('<input required type="radio" value='.$query_row["optionOne"].' name="option">'). ' '. $query_row['optionOne'] . '<br/>';

					print ('<input required type="radio"  value='.$query_row["optionTwo"].' name="option">').' '. $query_row['optionTwo'] . '<br/>';

					print ('<input required type="radio" value='.$query_row["optionThree"].' name="option">'). ' '. $query_row['optionThree'] . '<br/>';

					print ('<input required type="radio" value='.$query_row["optionFour"].' name="option">'). ' '. $query_row['optionFour'] . '<br/>' . '<br/>';
				}

				//code for previous button
				$prev = $page - 1;
				$next = $page + 1;

				if (!($page <= 1)) {
					print "<a class='w3-btn w3-teal w3-border w3-border-tear w3-round-large' href='exam.php?success=$name&page=$prev&name=$name'>Previous</a> " .' ';
				}

				
				//code for next button

				if (!($page >= $pages)) {
					print " <a class='w3-btn w3-teal w3-border w3-border-tear w3-round-large' href='exam.php?success=$name&page=$next&name=$name'>Next</a>". '<br />'. '<br/>';
				}

				//to loop out the remaining page as a link

				if ($page >= 1) {

					for ($i=1; $i <= $pages; $i++) {

						print ($i == $page) ? '<b><a href="?page='.$i.'&success='.$name.'">'.$i.'</a></b> ' : '<a href="?page='.$i.'&success='.$name.'">'.$i.'</a> ';
					}

				}

			?>

			<br> <br> <button class="Sbtn w3-btn w3-round w3-green" type="submit" name="result">Submit</button>
		</form>

		<form action="logout/logout.php" method="POST">

			<button class=" w3-btn w3-red w3-hover-red w3-padding" style="margin-left: 5px;" type="submit" name="end">End Quiz</button>
		</form>

		<hr class="line-up" size="">

</div>

</body>
</html>