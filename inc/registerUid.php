<?php

	require_once "server.php";

	if (isset($_POST['register'])) {
		
		$firstName = mysqli_real_escape_string($dbConnection, $_POST['firstName']);
		$lastName = mysqli_real_escape_string($dbConnection, $_POST['lastName']);
		$mail = mysqli_real_escape_string($dbConnection, $_POST['mail']);
		$gender = $_POST['gender'];
		$pwd = mysqli_real_escape_string($dbConnection, $_POST['pwd']);
		$re_pwd = mysqli_real_escape_string($dbConnection, $_POST['re_pwd']);

		$values = (empty($firstName) || empty($lastName) || empty($mail) || empty($gender) || empty($pwd) || empty($re_pwd)) ? true : false ;

		if ($values == true) {
			
			$_SESSION['Eerror'] = "Sorry seems some fields are empty";
			header('location: ../register.php?error=emptyfields');
			exit();

		} else {

			if ($pwd != $re_pwd) {
				
				$_SESSION['Perror'] = "Sorry it seems your Password does't not match";
				header('location: ../register.php?error=passwordmissmatch');
				exit();

			} else {

				if (strlen($pwd) <= 8 && strlen($re_pwd) <= 8) {
					
					$_SESSION['Werror'] = "Sorry it seems you password is not strong enough";
					header('location: ../register.php?error=weakpassword');
					exit();

				} else {

					$sql = "INSERT INTO student(firstName, lastName, mail, gender, pwd) VALUES('$firstName', '$lastName', '$mail', '$gender', '$pwd')";
					$bind = mysqli_query($dbConnection, $sql) or die('Error from bind on line 41');
					header('location: ../index.php?success');
					exit();
				}
			}
		}
	}

	