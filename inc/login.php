<?php

	require_once "server.php";
	session_start();

	if (isset($_POST['login'])) {
		
		$uid = mysqli_real_escape_string($dbConnection, $_POST['uid']);
		$pwd = mysqli_real_escape_string($dbConnection, $_POST['pwd']);
		$id = mysqli_real_escape_string($dbConnection, $_POST['id']);

		$values = (empty($uid) || empty($pwd)) ? true : false ;

		if ($values == true) {
			
			$_SESSION['Eerror'] = "Sorry it seems some fields are empty";
			header('location: ../index.php?error=emptyfields');
			exit();

		} else {

			$sql = "SELECT * FROM student WHERE firstName='$uid'";
			$bind = mysqli_query($dbConnection, $sql) or die('Error from bind on line 21');
			$count = mysqli_num_rows($bind);

			if (!$count) {
				
				$_SESSION['Uerror'] = "Sorry seems this name is not registered Please register";
				header('location: ../index.php?error=userdoesnotexist');
				exit();

			} else {

				$sqlP = "SELECT pwd FROM student WHERE pwd='$pwd'";
				$bindP = mysqli_query($dbConnection, $sqlP) or die('Error from bind P on line 34');
				$check = mysqli_fetch_array($bindP);

				if ($check['pwd'] != $pwd) {
					
					$_SESSION['Perror'] = "Sorry it seems your password is not correct";
					header('location: ../index.php?error=wrongpassword');
					exit();

				} else {

					$_SESSION['welcome'] = "Successfully Logged In";
					header('location: ../exam.php?success='.$uid);
					exit();
				}
			}
		}
	}