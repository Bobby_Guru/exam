<?php

	require_once "server.php";

	if (isset($_POST['login'])) {
		
		$uid = mysqli_real_escape_string($dbConnection, $_POST['uid']);
		$pwd = mysqli_real_escape_string($dbConnection, $_POST['pwd']);

		if (empty($uid) || empty($pwd)) {
			
			$_SESSION['Eerror'] = "Sorry it seems some fields are emptys";
			header('location: ../admin.php?error=emptyfields');
			exit();

		} else {

			$sql = "SELECT uid FROM admin WHERE uid='$uid'";
			$bind = mysqli_query($dbConnection, $sql) or die('Error from bind on line 19');
			$count = mysqli_num_rows($bind);

			if (!$count) {
				
				$_SESSION['Uerror'] = "Sorry seems this is not the Admins name";
				header('location: ../admin.php?error=userdoesnotexist');
				exit();

			} else {

				$sqlP = "SELECT pwd FROM admin WHERE pwd='$pwd'";
				$bindP = mysqli_query($dbConnection, $sqlP) or die('Error from bind P on line 30');
				$check = mysqli_fetch_array($bindP);

				if ($check['pwd'] != $pwd) {
					
					$_SESSION['Perror'] = "Sorry it seems your password is not correct";
					header('location: ../admin.php?error=wrongpassword');
					exit();

				} else {

					$_SESSION['welcome'] = "Successfully Logged In";
					header('location: ../adminPanel.php?success='.$uid);
					exit();
				}
			}

		}
	}