<?php
	//session_start();
	require_once "inc/fetch.php";
	require_once "inc/login.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Online Examination</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">

	<nav>
		<ul class="w3-navbar w3-black w3-mobile">
			<li><a href="index.php">Home</a></li>
			<li><a href="admin.php">Admin</a></li>
			<li><a href="register.php">Register</a></li>
		</ul>
	</nav>

</head>
<body class="w3-mobile" background="images/bd.jpg">

	<header class="main-head w3-head w3-container w3-red">
		<h1 class="w3-center head-text">Online Examination for Auchi Polytechnic <br> Staff Recriuptment</h1>
	</header>

	<?php
		if (isset($_SESSION['Eerror'])) {?>
			<center>
			<div class="error">
				<?php
					print $_SESSION['Eerror'];
					unset($_SESSION['Eerror']);
				?>
			</div>
			</center>
	<?php }?>

	<?php
		if (isset($_SESSION['Uerror'])) {?>
			<center>
			<div class="error">
				<?php
					print $_SESSION['Uerror'];
					unset($_SESSION['Uerror']);
				?>
			</div>
			</center>
	<?php }?>

	<?php
		if (isset($_SESSION['Perror'])) {?>
			<center>
			<div class="error">
				<?php
					print $_SESSION['Perror'];
					unset($_SESSION['Perror']);
				?>
			</div>
			</center>
	<?php }?>

	<center>
	<div class="form-body w3-padding">
		<form action="inc/login.php" method="POST">
			<input type="hidden" name="id" value="<?php print $row['id']; ?>">
			
			<div>
				<label class="lText w3-label" for="name">First Name</label> <br>
				<input class="input" type="text" name="uid" id="name">
			</div>

			<div>
				<label class="lText w3-label" for="pwd">Password</label> <br>
				<input class="input" type="password" name="pwd" id="pwd">
			</div>

			<button class="btn w3-btn w3-teal w3-round-large w3-mobile" type="submit" name="login">Login</button>

		</form>

		<p>Not yet Registered? Please Click here to <a href="register.php">Register</a></p>
	</div>
	</center>

	<footer class="footer w3-footer w3-red w3-center w3-padding w3-bottom">
		Copyright &copy; 2019 Auchi Polytechnic ND Project
	</footer>

</body>
</html>