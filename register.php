<?php
	session_start();
	require_once "inc/registerUid.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Online Examination</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">

	<nav>
		<ul class="w3-navbar w3-black">
			<li><a href="index.php">Home</a></li>
			<li><a href="admin.php">Admin</a></li>
			<li><a href="index.php">Login</a></li>
		</ul>
	</nav>

</head>
<body class="" background="images/bd.jpg">

	<header class="main-head w3-container w3-red">
		<h1 class="w3-center head-text">Online Examination for Auchi Polytechnic <br> Staff Recriuptment</h1>
	</header>

	<center>
		<div class="form-body w3-container w3-text-grey">
			<form action="inc/registerUid.php" method="POST">
				<div>
					<label class="w3-label" for="first">First Name</label><br>
					<input class="input" type="text" name="firstName" id="first">
				</div>

				<div>
					<label class="w3-label" for="last">Last Name</label><br>
					<input class="input" type="text" name="lastName" id="last">
				</div>

				<div>
					<label class="w3-label" for="mail">Email</label><br>
					<input class="input" type="text" name="mail" id="mail">
				</div>

				<div>
					<label class="w3-label" for="first">Gender</label><br>
					<input type="radio" required name="gender" id="male" value="male"><label for="male"> Male</label> |
					<input type="radio" required name="gender" id="female" value="female"><label for="female"> Female</label>
				</div>

				<div>
					<label class="w3-label" for="pwd">Password</label><br>
					<input class="input" type="password" name="pwd" id="pwd">
				</div>

				<div>
					<label class="w3-label" for="re_pwd">Repeat Password</label><br>
					<input class="input" type="password" name="re_pwd" id="re_pwd">
				</div>

				<button class="btn w3-btn w3-mobile w3-teal w3-round-large" type="submit" name="register">Register</button>

			</form>

			<p>Already have an Account? Please Click here to <a href="index.php">Login</a></p>
		</div>
	</center>

	<footer class="foot w3-footer w3-red w3-padding w3-center">
		Copyright &copy; 2019 Auchi Polytechnic ND Project
	</footer>

</body>
</html>