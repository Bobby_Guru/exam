<?php

	require_once "server.php";

	if (isset($_POST['ques'])) {
		
		$ques = mysqli_real_escape_string($dbConnection, $_POST['questions']);
		$option1 = mysqli_real_escape_string($dbConnection, $_POST['optionOne']);
		$option2 = mysqli_real_escape_string($dbConnection, $_POST['optionTwo']);
		$option3 = mysqli_real_escape_string($dbConnection, $_POST['optionThree']);
		$option4 = mysqli_real_escape_string($dbConnection, $_POST['optionFour']);
		$ans = mysqli_real_escape_string($dbConnection, $_POST['ans']);
		$id = mysqli_real_escape_string($dbConnection, $_POST['id']);

		$fields = (empty($ques) || empty($option1) || empty($option2) || empty($option3) || empty($option4) || empty($ans)) ? true : false ;

		if ($fields) {
			
			header('location: ../editq.php?edit=$id');
			exit();
		}
	}