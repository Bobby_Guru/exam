<?php
	
	session_start();

	include_once 'server.php';

	if (isset($_POST['ques'])) {

		$questions = mysqli_real_escape_string($dbConnection, $_POST['questions']);
		$option1 = mysqli_real_escape_string($dbConnection, $_POST['optionOne']);
		$option2 = mysqli_real_escape_string($dbConnection, $_POST['optionTwo']);
		$option3 = mysqli_real_escape_string($dbConnection, $_POST['optionThree']);
		$option4 = mysqli_real_escape_string($dbConnection, $_POST['optionFour']);
		$answer = mysqli_real_escape_string($dbConnection, $_POST['ans']);

		if (empty($questions)) {
			header('location: ../addq.php?error=emptyfields');
			exit();
		} else {
			$query = "INSERT INTO questions(question, optionOne, optionTwo, optionThree, optionFour, answer) VALUES('$questions', '$option1', '$option2', '$option3', '$option4', '$answer')";
			$con = mysqli_query($dbConnection, $query) or die('con error on line 21');
			header('location: ../addq.php?success=contentadded');
			exit();
		}
	}

	session_unset();
	session_destroy();