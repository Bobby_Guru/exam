<?php

	include "inc/server.php";

	$sql = "SELECT * FROM questions";
	$bind = mysqli_query($dbConnection, $sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Questions</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">

	<nav>
		<ul class="w3-navbar w3-black">
			<li><a href="adminPanel.php">View Results</a></li>
			<li><a href="addq.php">Add Questions</a></li>
			<li><a href="viewq.php">View Questions</a></li>
			<li><a href="">Update Profile</a></li>
			<li><a href="logout/logout.php?out">Logout</a></li>
		</ul>
	</nav>
</head>
<body class="w3-container">

	<div id="about-container w3-container">
		<h1 class="qHead">Admin C Panel</h1>

		<h4 class="qHead">View and Edit Questions</h4>

		<div>
			<form action="" method="POST">
				<table class="w3-table w3-bordered w3-border w3-hoverable">
					<thead>
						<tr>
							<td>Questions</td>
							<td>Option 1</td>
							<td>Option 2</td>
							<td>Option 3</td>
							<td>Option 4</td>
							<td>Answer</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<?php while ($row = mysqli_fetch_array($bind)) {?>
						<tr>
							<td><?= $row['question']; ?></td>
							<td><?= $row['optionOne']; ?></td>
							<td><?= $row['optionTwo']; ?></td>
							<td><?= $row['optionThree']; ?></td>
							<td><?= $row['optionFour']; ?></td>
							<td><?= $row['answer']; ?></td>
							<td>
								<a class="w3-btn w3-blue w3-round-large" href="editq.php?edit=<?php print $row['id']; ?>">Edit</a>
							</td>
							<td>
								<a class="w3-btn w3-red w3-round-large" href="">Delete</a>
							</td>
						</tr>
					<?php }?>
					</tbody>
				</table>
			</form>
			
		</div>

	</div>


</body>
</html>