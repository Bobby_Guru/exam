<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/indexStyle.css">
</head>
<body>

	<center>
	<div style="margin-top: 50px;" class="form-body w3-container w3-padding">
		<h2>Admin Login Permit</h2>
		<form action="admin/login.php" method="POST">
			<div>
				<label for="username">Username</label> <br>
				<input class="input" type="text" name="uid" id="username">
			</div>

			<div>
				<label for="password">Password</label> <br>
				<input class="input" type="password" name="pwd" id="password">
			</div> <br>

			<button class="w3-btn w3-teal w3-round-large" type="submit" name="login">Login</button>
		</form>
	</div>
	</center>
</body>
</html>